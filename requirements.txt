cffi==1.14.6
click==8.0.1
colorama==0.4.4
cryptography==3.4.8
pycparser==2.20
pyscard==2.0.2
